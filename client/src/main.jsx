import ReactDOM from "react-dom"
import App from "./App.jsx"

const appcontainer = document.getElementById('myrailsapp');

if (appcontainer !== null) {
	ReactDOM.render(<App/>, appcontainer)
}