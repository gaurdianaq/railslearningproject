const gulp = require("gulp");
const browserify = require("browserify");
const watchify = require("watchify");
const fancy_log = require("fancy-log");
const source = require("vinyl-source-stream");

const OUTPUT_FOLDER = "../public"

function copy_html() {
	return gulp.src("src/*.html").pipe(gulp.dest(OUTPUT_FOLDER));
}

const watchedBrowserify = watchify(
	browserify({
		basedir: ".",
		debug: true,
		entries: ["src/main.jsx"],
		cache: {},
		packageCache: {}
	}).transform("babelify", {presets: ["@babel/preset-env", ["@babel/preset-react", {"runtime": "automatic"}]]})
);

function debug_build() {
	return watchedBrowserify
		.bundle()
		.on("error", fancy_log)
		.pipe(source("app.js"))
		.pipe(gulp.dest(OUTPUT_FOLDER));
}

exports.debug = gulp.parallel(copy_html, debug_build);