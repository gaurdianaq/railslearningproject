class CreatePurchases < ActiveRecord::Migration[6.0]
  def change
    #purchases do not need the updated_at column since purchases should not be updated
    create_table :purchases do |t|
      t.belongs_to :product, foreign_key: true, null: false
      t.belongs_to :user, foreign_key: true, null: false
      t.datetime :purchase_date, null: false, default: DateTime.now
    end
  end
end
