class CreateInitialUsers < ActiveRecord::Migration[6.0]
  def up
    User.create(name: "Evan")
    User.create(name: "George")
    User.create(name: "Fred")
    User.create(name: "Bob")
    User.create(name: "Sheila")
  end
  def down
    User.destroy_by(name = "Evan")
    User.destroy_by(name = "George")
    User.destroy_by(name = "Fred")
    User.destroy_by(name = "Bob")
    User.destroy_by(name = "Sheila")
  end
end
