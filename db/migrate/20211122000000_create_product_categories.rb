class CreateProductCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :product_categories, id: false do |t|
      t.string :name, primary_key: true, null: false
    end
  end
end
