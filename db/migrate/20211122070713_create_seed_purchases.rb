class CreateSeedPurchases < ActiveRecord::Migration[6.0]
  def up
    Purchase.create(product_id: 1, user_id: 1)
    Purchase.create(product_id: 1, user_id: 2)
    Purchase.create(product_id: 3, user_id: 1)
    Purchase.create(product_id: 3, user_id: 1)
  end
  def down
    Purchase.destroy_all
  end
end
