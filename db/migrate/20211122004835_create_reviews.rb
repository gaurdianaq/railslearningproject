class CreateReviews < ActiveRecord::Migration[6.0]
  def change
    create_table :reviews do |t|
      t.belongs_to :product, foreign_key: true, null: false
      t.belongs_to :user, foreign_key: true, null: false
      t.text :message, null: false
      t.timestamps
    end
  end
end
