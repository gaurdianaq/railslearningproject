class CreateInitialProducts < ActiveRecord::Migration[6.0]
  def up
    Product.create(name: "Book 1", price: 5.99, product_category_id: "Book")
    Product.create(name: "Book 2", price: 9.99, product_category_id: "Book")
    Product.create(name: "Computer 1", price: 599.99, product_category_id: "Computer")
    Product.create(name: "Computer 2", price: 1099.99, product_category_id: "Computer")
    Product.create(name: "Bed 1", price: 1000.99, product_category_id: "Furniture")
    Product.create(name: "Bed 2", price: 2000.99, product_category_id: "Furniture")
    Product.create(name: "Apple 1", price: 1.99, product_category_id: "Food")
    Product.create(name: "Apple 2", price: 2.99, product_category_id: "Food")
  end
end
