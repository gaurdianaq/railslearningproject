class AddInitialProductCategories < ActiveRecord::Migration[6.0]
  def up
    ProductCategory.create(name: "Book")
    ProductCategory.create(name: "Computer")
    ProductCategory.create(name: "Furniture")
    ProductCategory.create(name: "Food")
  end

  def down
    ProductCategory.destroy("Book")
    ProductCategory.destroy("Computer")
    ProductCategory.destroy("Furniture")
    ProductCategory.destroy("Food")
  end
end
