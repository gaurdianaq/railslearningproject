class CreateSeedReviews < ActiveRecord::Migration[6.0]
  def up
    Review.create(product_id: 1, user_id: 1, message: "Worst book I've ever read, can't believe I paid money for this trash!")
    Review.create(product_id: 1, user_id: 2, message: "Best book I've ever read, would highly recommend!")
    Review.create(product_id: 3, user_id: 1, message: "This computer is utter garbage!")
    Review.create(product_id: 7, user_id: 1, message: "Who can even stand apple 1? apple 2 is clearly the superior apple!")
  end
  def down
    Review.destroy_all
  end
end
