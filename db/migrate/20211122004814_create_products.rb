class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    #
    create_table :products do |t|
      t.string :name, null: false
      t.float :price, null: false
      t.belongs_to :product_category, foreign_key: {primary_key: :name}, type: :string
    end
  end
end
