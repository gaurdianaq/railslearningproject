module Types
  class PurchaseType < Types::BaseObject
    field :product, ProductType, null: false, method: :product_id
    def product
      Product.find(object.product_id)
    end
    field :user, UserType, null: false, method: :user_id
    def user
      User.find(object.user_id)
    end
    field :purchase_date, Integer, null: false
  end
end
