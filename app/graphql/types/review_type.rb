module Types
	class ReviewType < Types::BaseObject
		field :product, Types::ProductType, null: false, method: :product_id
		def product
			Product.find(object.product_id)
		end
		field :user, Types::UserType, null: false, method: :user_id
		def user
			User.find(object.user_id)
		end
		field :message, String, null: false
		field :created_at, Integer, null: false
		field :updated_at, Integer, null: true
	end
end
