module Types
	class ProductCategoryType < Types::BaseEnum
		value "BOOK", value: "Book"
		value "COMPUTER", value: "Computer"
		value "FURNITURE", value: "Furniture"
		value "FOOD", value: "Food"
	end
end