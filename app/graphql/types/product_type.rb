module Types
	class ProductType < Types::BaseObject
		description "A product you can buy at the store"
		field :id, ID, null: false
		field :name, String, null: false
		field :price, Float, null: false
		field :category, Types::ProductCategoryType, null: false, method: :product_category_id
		field :reviews, [ReviewType], null: false
		def reviews
			Review.where(product_id: object.id)
		end
	end
end