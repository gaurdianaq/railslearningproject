module Types
  class QueryType < Types::BaseObject
    # Add `node(id: ID!) and `nodes(ids: [ID!]!)`
    include GraphQL::Types::Relay::HasNodeField
    include GraphQL::Types::Relay::HasNodesField

    field :get_all_products, [Types::ProductType], null: false
    def get_all_products
      Product.all
    end

    field :get_all_users, [Types::UserType], null: false
    def get_all_users
      User.all
    end

    field :get_all_purchases, [Types::PurchaseType], null: false
    def get_all_purchases
      Purchase.all
    end

    field :get_all_user_reviews, [Types::ReviewType], null: false do
      argument :user_id, Integer, required: true
    end
    def get_all_user_reviews(user_id:)
      Review.where(user_id: user_id)
    end

    field :get_all_product_reviews, [Types::ReviewType], null: false do
      argument :product_id, Integer, required: true
    end
    def get_all_product_reviews(product_id:)
      Review.where(product_id: product_id)
    end

    field :get_all_user_purchases, [Types::PurchaseType], null: false do
      argument :user_id, Integer, required: true
    end
    def get_all_user_purchases(user_id:)
      Purchase.where(user_id: user_id)
    end
  end
end
