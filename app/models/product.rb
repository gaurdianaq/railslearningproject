class Product < ApplicationRecord
	has_many :purchases
	has_many :reviews
end
