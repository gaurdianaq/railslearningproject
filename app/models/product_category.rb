class ProductCategory < ApplicationRecord
	self.primary_key = "name"
	has_many :products
end
